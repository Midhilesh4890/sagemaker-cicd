#!/usr/bin/env python

import os
import joblib
import requests
import json
from datetime import datetime, timezone
import pandas as pd
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error
import boto3
import botocore
import logging

# Configure logging
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s - %(levelname)s - %(message)s')


def update_report_file(metrics, hyperparameters, commit_hash, training_job_name, prefix, bucket_name):
    """
    Updates or creates a report file in the specified S3 bucket with training job metrics.
    """
    object_key = f'{prefix}/reports.csv'
    s3 = boto3.resource('s3')

    try:
        s3.Bucket(bucket_name).download_file(object_key, 'reports.csv')
        reports_df = pd.read_csv('reports.csv')
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == '404':
            logging.info("Report file not found, creating a new one.")
            columns = ['date_time', 'hyperparameters', 'commit_hash',
                       'training_job_name'] + list(metrics.keys())
            pd.DataFrame(columns=columns).to_csv('reports.csv', index=False)
            s3.Bucket(bucket_name).upload_file('reports.csv', object_key)
            reports_df = pd.read_csv('reports.csv')
        else:
            logging.error("Error downloading report file: %s", e)
            raise

    date_time = datetime.now(timezone.utc).strftime("%Y-%m-%d %H:%M:%S")
    new_row = {'date_time': date_time,
               'hyperparameters': json.dumps(hyperparameters),
               'commit_hash': commit_hash,
               'training_job_name': training_job_name,
               **metrics}
    new_report_df = pd.DataFrame([new_row])
    reports_df = pd.concat([reports_df, new_report_df], ignore_index=True)
    reports_df.to_csv('reports.csv', index=False)
    s3.Bucket(bucket_name).upload_file('reports.csv', object_key)
    logging.info("Report file updated successfully.")


def load_data(file_path):
    """
    Loads CSV data from a given file path.
    """
    return pd.read_csv(file_path)


def load_configurations():
    """
    Loads configuration files and environment variables.
    """
    with open('/opt/ml/input/config/hyperparameters.json', 'r') as file:
        hyperparameters = json.load(file)
    logging.info("Hyperparameters: %s", hyperparameters)

    with open('/opt/ml/input/config/inputdataconfig.json', 'r') as file:
        input_data_config = json.load(file)
    logging.info("Input Data Config: %s", input_data_config)

    with open('/opt/ml/input/config/resourceconfig.json', 'r') as file:
        resource_config = json.load(file)
    logging.info("Resource Config: %s", resource_config)

    return hyperparameters


def train_model(X_train, y_train, hyperparameters):
    """
    Trains a RandomForestRegressor model with the given training data and hyperparameters.
    """
    n_estimators = int(hyperparameters['nestimators'])
    model = RandomForestRegressor(n_estimators=n_estimators)
    model.fit(X_train, y_train)
    logging.info("Model training completed.")
    return model


def evaluate_model(model, X_train, y_train, X_val, y_val):
    """
    Evaluates the model on training and validation data.
    """
    train_mse = mean_squared_error(y_train, model.predict(X_train))
    val_mse = mean_squared_error(y_val, model.predict(X_val))
    metrics = {'Train_MSE': train_mse, 'Validation_MSE': val_mse}
    logging.info("Model evaluation metrics: %s", metrics)
    return metrics


def save_model(model, output_path):
    """
    Saves the trained model to the specified output path.
    """
    joblib.dump(model, output_path)
    logging.info("Model saved to %s", output_path)


def post_report_to_gitlab(metrics, bucket_name, region, prefix):
    """
    Posts the training job performance report to the GitLab merge request.
    """
    report_message = (
        f"\n## Training Job Performance Report\n\n"
        f"{pd.DataFrame([metrics]).to_markdown(index=False)}\n\n"
        "Full History of Performance reports:\n\n"
        f"[View Report on S3](https://s3.console.aws.amazon.com/s3/object/{bucket_name}?region={region}&prefix={prefix}/reports.csv)\n\n"
    )

    response = requests.post(
        f"https://gitlab.com/api/v4/projects/{os.getenv('CI_PROJECT_ID')}/merge_requests/{os.getenv('CI_MERGE_REQUEST_IID')}/notes",
        headers={"PRIVATE-TOKEN": os.getenv('GITLAB_ACCESS_TOKEN')},
        data={"body": report_message}
    )
    if response.status_code == 201:
        logging.info("Report posted to GitLab merge request successfully.")
    else:
        logging.error(
            f"Failed to post report to GitLab. Status code: {response.status_code}")


def main():
    hyperparameters = load_configurations()

    training_data_path = '/opt/ml/input/data/training/boston-housing-training.csv'
    validation_data_path = '/opt/ml/input/data/validation/boston-housing-validation.csv'
    training_data = load_data(training_data_path)
    validation_data = load_data(validation_data_path)

    X_train, y_train = training_data.iloc[:, 1:], training_data.iloc[:, 0]
    X_val, y_val = validation_data.iloc[:, 1:], validation_data.iloc[:, 0]

    model = train_model(X_train, y_train, hyperparameters)
    metrics = evaluate_model(model, X_train, y_train, X_val, y_val)

    model_output_path = '/opt/ml/model/model.joblib'
    save_model(model, model_output_path)

    update_report_file(
        metrics=metrics,
        hyperparameters=hyperparameters,
        commit_hash=os.getenv('CI_COMMIT_SHORT_SHA'),
        training_job_name=os.getenv('TRAINING_JOB_NAME'),
        prefix=os.getenv('PREFIX'),
        bucket_name=os.getenv('BUCKET_NAME')
    )

    post_report_to_gitlab(
        metrics=metrics,
        bucket_name=os.getenv('BUCKET_NAME'),
        region=os.getenv('REGION'),
        prefix=os.getenv('PREFIX')
    )


if __name__ == '__main__':
    main()
