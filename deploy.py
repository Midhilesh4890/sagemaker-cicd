#!/usr/bin/env python

import os
import pandas as pd
import boto3
import botocore
import logging
from sagemaker.estimator import Estimator

# Configure logging
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s - %(levelname)s - %(message)s')


class SagemakerDeployment:
    def __init__(self, bucket_name, object_key, instance_count, instance_type):
        self.bucket_name = bucket_name
        self.object_key = object_key
        self.instance_count = instance_count
        self.instance_type = instance_type
        self.s3 = boto3.resource('s3')

    def download_report(self):
        """Download the report from S3 and load it into a DataFrame."""
        try:
            self.s3.Bucket(self.bucket_name).download_file(
                self.object_key, 'reports.csv')
            reports_df = pd.read_csv('reports.csv')
            logging.info("Report downloaded and loaded successfully.")
            return reports_df
        except botocore.exceptions.ClientError as e:
            if e.response['Error']['Code'] == '404':
                logging.error("Report.csv not found in the bucket.")
            else:
                logging.exception(
                    "Unexpected error occurred while downloading the report.")
                raise

    def deploy_model(self, reports_df):
        """Sort DataFrame by date, attach to the latest estimator, and deploy the model."""
        reports_df['date_time'] = pd.to_datetime(
            reports_df['date_time'], format='%Y-%m-%d %H:%M:%S')
        latest_training_job_name = reports_df.sort_values(
            ['date_time'], ascending=False).iloc[0]['training_job_name']
        attached_estimator = Estimator.attach(latest_training_job_name)

        attached_predictor = attached_estimator.deploy(initial_instance_count=self.instance_count,
                                                       instance_type=self.instance_type,
                                                       endpoint_name=latest_training_job_name,
                                                       tags=[
                                                           {"Key": "email", "Value": os.getenv('email_address')}],
                                                       wait=False)
        logging.info(
            f"Model deployed. Endpoint name: {attached_predictor.endpoint_name}")


if __name__ == '__main__':
    bucket_name = os.getenv('BUCKET_NAME')
    prefix = os.getenv('PREFIX')
    object_key = f'{prefix}/reports.csv'
    initial_instance_count = 1
    endpoint_instance_type = 'ml.m5.large'

    deployment = SagemakerDeployment(
        bucket_name, object_key, initial_instance_count, endpoint_instance_type)
    reports_df = deployment.download_report()
    if reports_df is not None:
        deployment.deploy_model(reports_df)
