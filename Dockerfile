# Use Python 3.8 official image as the base image
FROM python:3.8

RUN pip3 install --no-cache-dir scikit-learn pandas joblib flask requests boto3 tabulate

WORKDIR app/
# Copy training and serving scripts to the container
COPY requirements.txt .

RUN pip install --no-cache-dir -r requirements.txt

CMD ["uvicorn", "serve-script:app", "--host", "0.0.0.0", "--port", "8080"]

