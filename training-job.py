#!/usr/bin/env python

import os
import requests
import sagemaker
from sagemaker.estimator import Estimator
import boto3
import logging

# Set up logging
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s - %(levelname)s - %(message)s')

# Create SageMaker session
session = sagemaker.Session(boto3.session.Session())

# Retrieve environment variables
BUCKET_NAME = os.getenv('BUCKET_NAME')
PREFIX = os.getenv('PREFIX')
REGION = os.getenv('AWS_DEFAULT_REGION')
CI_COMMIT_SHORT_SHA = os.getenv('CI_COMMIT_SHORT_SHA')
CI_PROJECT_ID = os.getenv('CI_PROJECT_ID')
CI_PROJECT_NAME = os.getenv('CI_PROJECT_NAME')
CI_MERGE_REQUEST_IID = os.getenv('CI_MERGE_REQUEST_IID')
GITLAB_ACCESS_TOKEN = os.getenv('GITLAB_ACCESS_TOKEN')
IAM_ROLE_NAME = 'SageMaker-CICD'  # Replace with your IAM role ARN
# Replace with your desired training instance type
TRAINING_INSTANCE = 'ml.m5.large'

# Define S3 paths for training and validation data
training_data_s3_uri = f's3://{BUCKET_NAME}/{PREFIX}/boston-housing-training.csv'
validation_data_s3_uri = f's3://{BUCKET_NAME}/{PREFIX}/boston-housing-validation.csv'
output_folder_s3_uri = f's3://{BUCKET_NAME}/{PREFIX}/output/'
source_folder = f's3://{BUCKET_NAME}/{PREFIX}/source-folders'

# Get AWS account ID
ACCOUNT_ID = session.boto_session.client(
    'sts').get_caller_identity()['Account']


def submit_training_job():
    """Submits a training job to SageMaker and returns the job name and hyperparameters."""
    boston_estimator = Estimator(
        image_uri=f'{ACCOUNT_ID}.dkr.ecr.{REGION}.amazonaws.com/{CI_PROJECT_NAME}:latest',
        role=IAM_ROLE_NAME,
        instance_count=1,
        instance_type=TRAINING_INSTANCE,
        output_path=output_folder_s3_uri,
        code_location=source_folder,
        base_job_name='boston-housing-model',
        hyperparameters={'nestimators': 50},
        environment={
            "CI_MERGE_REQUEST_IID": CI_MERGE_REQUEST_IID,
            "GITLAB_ACCESS_TOKEN": GITLAB_ACCESS_TOKEN,
            "BUCKET_NAME": BUCKET_NAME,
            "PREFIX": PREFIX,
            "CI_COMMIT_SHORT_SHA": CI_COMMIT_SHORT_SHA,
            "CI_PROJECT_ID": CI_PROJECT_ID,
            "REGION": REGION,
        },
        tags=[{"Key": "email", "Value": os.getenv('email_address')}]
    )

    boston_estimator.fit({'training': training_data_s3_uri,
                         'validation': validation_data_s3_uri}, wait=False)

    training_job_name = boston_estimator.latest_training_job.name
    hyperparameters_dict = boston_estimator.hyperparameters()

    logging.info(f"Training job '{training_job_name}' submitted.")
    return training_job_name, hyperparameters_dict


def post_message_to_gitlab(training_job_name, hyperparameters_dict):
    """Posts a message to the GitLab merge request page with training job details."""
    model_artifacts_uri = f's3://{BUCKET_NAME}/{PREFIX}/output/{training_job_name}/output/model.tar.gz'
    cloudwatch_logs_uri = f'https://{REGION}.console.aws.amazon.com/cloudwatch/home?region={REGION}#logStream:group=/aws/sagemaker/TrainingJobs;prefix={training_job_name}'
    endpoint_uri = f'https://runtime.sagemaker.{REGION}.amazonaws.com/endpoints/{training_job_name}/invocations'

    message = (f"## Training Job Submission Report\n\n"
               f"Training Job name: '{training_job_name}'\n\n"
               f"Model Artifacts Location:\n\n'{model_artifacts_uri}'\n\n"
               f"Model hyperparameters: {hyperparameters_dict}\n\n"
               f"See the logs at: [CloudWatch]({cloudwatch_logs_uri})\n\n"
               f"If you merge this pull request, the resulting endpoint will be available at:\n\n'{endpoint_uri}'\n\n")

    response = requests.post(
        f"https://gitlab.com/api/v4/projects/{CI_PROJECT_ID}/merge_requests/{CI_MERGE_REQUEST_IID}/notes",
        headers={"PRIVATE-TOKEN": GITLAB_ACCESS_TOKEN},
        data={"body": message}
    )

    if response.status_code == 201:
        logging.info("Message posted to GitLab merge request successfully.")
    else:
        logging.error(
            f"Failed to post message to GitLab merge request. Status code: {response.status_code}")


def main():
    try:
        training_job_name, hyperparameters_dict = submit_training_job()
        post_message_to_gitlab(training_job_name, hyperparameters_dict)
    except Exception as e:
        logging.exception(
            "An error occurred during the training job submission or GitLab notification process.")


if __name__ == "__main__":
    main()
