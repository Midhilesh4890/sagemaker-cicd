#!/usr/bin/env python

import joblib
import os
from fastapi import FastAPI, HTTPException, Request
from fastapi.responses import PlainTextResponse
import pandas as pd
import uvicorn

# Initialize FastAPI app
app = FastAPI()

# Load the pre-trained model from the designated path
model_path = '/opt/ml/model'
model = joblib.load(os.path.join(model_path, "model.joblib"))


@app.get("/ping")
async def ping():
    """Health check route to ensure the API is up."""
    return PlainTextResponse(content="\n", status_code=200)


@app.post("/invocations")
async def predict(request: Request):
    """Endpoint for making predictions with the model."""
    if request.headers['content-type'] == 'text/csv':
        data = await request.body()
        data = data.decode('utf-8')
        data_df = pd.read_csv(pd.StringIO(data), header=None)

        # Assuming the model expects data in a specific format, for example, all rows:
        response = model.predict(data_df.values)
        response_df = pd.DataFrame(response)
        csv_output = response_df.to_csv(header=False, index=False)
        return PlainTextResponse(content=csv_output, status_code=200)
    else:
        raise HTTPException(
            status_code=415, detail="Unsupported Media Type. CSV data only.")

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8080)
