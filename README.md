# SageMaker End2End CI/CD pipeline
A CI/CD pipeline with SageMaker and GitLab that helps you to collaborate on a Machine Learning project from training to deployment.

## Repository Variables
To work with this project you need to define following Repository Variables. Repository Variables can be defined from Setting->CI/CD->Variables.

1. AWS_ACCESS_KEY_ID
2. AWS_SECRET_ACCESS_KEY
3. AWS_DEFAULT_REGION: e.g. "eu-west-1"
4. GITLAB_ACCESS_TOKEN: the GitLab access token that has api access. Tokens can be created from edit profile->Access Tokens
5. AWS_SESSION_TOKEN: (Optional if you are not using temporary access key)
6. BUCKET_NAME: name of your S3 bucket
7. PREFIX: name of your project that is a subfolder of the S3 bucket
